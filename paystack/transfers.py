import json
from enum import IntFlag
from typing import Any
import requests

from paystack.util import PayStackGatewayFlag, BusinessDataObject, PayStackRestUrl, Command


__all__ = ('create_transfer_recipient', 'list_transfer_recipients',
           'update_transfer_recipient', 'delete_transfer_recipient', 'initiate_transfer',
           'list_transfers', 'fetch_transfer', 'finalize_transfer', 'initiate_bulk_transfer', 'verify_transfer')


class TransferCommandRank(IntFlag):
    CREATE_TRANSFER_RECIPIENT = 241
    LIST_TRANSFER_RECIPIENTS = 251
    UPDATE_TRANSFER_RECIPIENT = 257
    DELETE_TRANSFER_RECIPIENT = 263
    INITIATE_TRANSFER = 269
    LIST_TRANSFERS = 271
    FETCH_TRANSFER = 277
    FINALIZE_TRANSFER = 281
    INITIATE_BULK_TRANSFER = 283
    VERIFY_TRANSFER = 293


def create_transfer_recipient_cmd(**kwargs) -> Any:
    bdo = BusinessDataObject(use_config=True, **kwargs)
    data = bdo.data
    url = bdo.url(PayStackRestUrl.CREATE_TRANSFER_RECIPIENT_URL)
    response = requests.post(url=url, data=json.dumps(data), headers=bdo.header)
    
    return response


def list_transfer_recipients_cmd(**kwargs) -> Any:
    bdo = BusinessDataObject(use_config=True, **kwargs)
    query = bdo.data
    url = bdo.url(PayStackRestUrl.LIST_TRANSFER_RECIPIENTS_URL)
    response = requests.get(url=url, params=query, headers=bdo.header)
    
    return response


def delete_transfer_recipient_cmd(**kwargs) -> Any:
    bdo = BusinessDataObject(use_config=True, **kwargs)
    data = bdo.data
    transfer_recipient_id = data.get('transfer_recipient_id') or data.get('id') or data.get('slug')
    url = bdo.url(PayStackRestUrl.DELETE_TRANSFER_RECIPIENT_URL)
    url += f'{transfer_recipient_id}'
    response = requests.delete(url=url, headers=bdo.header)
    
    return response


def update_transfer_recipient_cmd(**kwargs) -> Any:
    bdo = BusinessDataObject(use_config=True, **kwargs)
    data = bdo.data
    transfer_recipient_id = data.get('transfer_recipient_id') or data.get('id') or data.get('slug')
    url = bdo.url(PayStackRestUrl.UPDATE_TRANSFER_RECIPIENT_URL)
    url += f'{transfer_recipient_id}'
    if 'transfer_recipient_id' in data:
        del data['transfer_recipient_id']
    elif 'id' in data:
        del data['id']
    elif 'slug' in data:
        del data['slug']
    response = requests.put(url=url, data=json.dumps(data), headers=bdo.header)
    
    return response


def initiate_transfer_cmd(**kwargs) -> Any:
    bdo = BusinessDataObject(use_config=True, **kwargs)
    data = bdo.data
    url = bdo.url(PayStackRestUrl.INITIATE_TRANSFER_URL)
    response = requests.post(url=url, data=json.dumps(data), headers=bdo.header)
    
    return response


def list_transfers_cmd(**kwargs) -> Any:
    bdo = BusinessDataObject(use_config=True, **kwargs)
    query = bdo.data
    url = bdo.url(PayStackRestUrl.LIST_TRANSFERS_URL)
    response = requests.get(url=url, params=query, headers=bdo.header)
    
    return response


def fetch_transfer_cmd(**kwargs) -> Any:
    bdo = BusinessDataObject(use_config=True, **kwargs)
    data = bdo.data
    url = bdo.url(PayStackRestUrl.FETCH_TRANSFER_URL)
    transfer_id = data.get('transfer_id') or data.get('id') or data.get('slug')
    url += f'{transfer_id}'
    response = requests.post(url=url, headers=bdo.header)
    
    return response


def finalize_transfer_cmd(**kwargs) -> Any:
    bdo = BusinessDataObject(use_config=True, **kwargs)
    data = bdo.data
    url = bdo.url(PayStackRestUrl.FINALIZE_TRANSFER_URL)
    response = requests.post(url=url, data=json.dumps(data), headers=bdo.header)
    
    return response


def initiate_bulk_transfer_cmd(**kwargs) -> Any:
    bdo = BusinessDataObject(use_config=True, **kwargs)
    data = bdo.data
    url = bdo.url(PayStackRestUrl.INITIATE_BULK_TRANSFER_URL)
    response = requests.post(url=url, data=json.dumps(data), headers=bdo.header)
    
    return response


def verify_transfer_cmd(**kwargs) -> Any:
    bdo = BusinessDataObject(use_config=True, **kwargs)
    data = bdo.data
    url = bdo.url(PayStackRestUrl.VERIFY_TRANSFER_URL)
    response = requests.get(url=url, params=json.dumps(data), headers=bdo.header)
    
    return response


create_transfer_recipient = Command(cmd=create_transfer_recipient_cmd,
                                    group=PayStackGatewayFlag.TRANSFER_RECIPIENTS,
                                    rank=TransferCommandRank.CREATE_TRANSFER_RECIPIENT,
                                    label=None)
list_transfer_recipients = Command(cmd=list_transfer_recipients_cmd,
                                   group=PayStackGatewayFlag.TRANSFER_RECIPIENTS,
                                   rank=TransferCommandRank.LIST_TRANSFER_RECIPIENTS,
                                   label=None)
update_transfer_recipient = Command(cmd=update_transfer_recipient_cmd,
                                    group=PayStackGatewayFlag.TRANSFER_RECIPIENTS,
                                    rank=TransferCommandRank.UPDATE_TRANSFER_RECIPIENT,
                                    label=None)
delete_transfer_recipient = Command(cmd=delete_transfer_recipient_cmd,
                                    group=PayStackGatewayFlag.TRANSFER_RECIPIENTS,
                                    rank=TransferCommandRank.DELETE_TRANSFER_RECIPIENT,
                                    label=None)
initiate_transfer = Command(cmd=initiate_transfer_cmd, group=PayStackGatewayFlag.TRANSFERS,
                            rank=TransferCommandRank.INITIATE_TRANSFER,
                            label=None)
list_transfers = Command(cmd=list_transfers_cmd, group=PayStackGatewayFlag.TRANSFERS,
                         rank=TransferCommandRank.LIST_TRANSFERS,
                         label=None)
fetch_transfer = Command(cmd=fetch_transfer_cmd, group=PayStackGatewayFlag.TRANSFERS,
                         rank=TransferCommandRank.FETCH_TRANSFER,
                         label=None)
finalize_transfer = Command(cmd=finalize_transfer_cmd, group=PayStackGatewayFlag.TRANSFERS,
                            rank=TransferCommandRank.FINALIZE_TRANSFER,
                            label=None)
initiate_bulk_transfer = Command(cmd=initiate_bulk_transfer_cmd,
                                 group=PayStackGatewayFlag.TRANSFERS,
                                 rank=TransferCommandRank.INITIATE_BULK_TRANSFER, label=None)

verify_transfer = Command(cmd=verify_transfer_cmd, group=PayStackGatewayFlag.TRANSFERS,
                          rank=TransferCommandRank.VERIFY_TRANSFER, label=None)
